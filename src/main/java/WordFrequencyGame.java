import java.util.*;

public class WordFrequencyGame {

    public static final int INITIAL_NUMBER_OF_WORDS = 1;
    public static final String LINE_BREAK = "\n";
    public static final String SPACE_REGEX = "\\s+";

    public String getResult(String inputStr) {

        try {
            List<Input> inputList = getInputList(inputStr);

            List<Input> wordCountPairs = countWords(inputList);

            wordCountPairs.sort((preWord, nextWord) -> nextWord.getWordCount() - preWord.getWordCount());

            return getResultString(wordCountPairs).toString();
        } catch (Exception e) {
            throw new CalculateErrorException();
        }
    }

    private static StringJoiner getResultString(List<Input> wordCountPairs) {
        StringJoiner outputResult = new StringJoiner(LINE_BREAK);
        wordCountPairs.forEach(wordCountPair -> {
            String singleOutput = wordCountPair.getValue() + " " + wordCountPair.getWordCount();
            outputResult.add(singleOutput);
        });
        return outputResult;
    }

    private List<Input> countWords(List<Input> inputList) {
        Map<String, Integer> wordCountMap = getListMap(inputList);
        List<Input> wordCountPairs = new ArrayList<>();
        wordCountMap.forEach((word, count) ->
                wordCountPairs.add(new Input(word, count)));
        return wordCountPairs;
    }

    private static List<Input> getInputList(String inputStr) {
        String[] words = inputStr.split(SPACE_REGEX);
        List<Input> inputList = new ArrayList<>();
        Arrays.stream(words).forEach(word ->
                inputList.add(new Input(word, INITIAL_NUMBER_OF_WORDS))
        );
        return inputList;
    }

    private Map<String, Integer> getListMap(List<Input> inputList) {
        Map<String, Integer> wordCountMap = new HashMap<>();
        inputList.forEach(input -> {
            if (!wordCountMap.containsKey(input.getValue())) {
                wordCountMap.put(input.getValue(), 1);
            } else {
                wordCountMap.put(input.getValue(), wordCountMap.get(input.getValue()) + 1);
            }
        });
        return wordCountMap;
    }


}
